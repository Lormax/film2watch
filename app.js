        var firebaseConfig = {
            apiKey: "AIzaSyCXcN_ks1Yto3o5Cus-8-PAN1L_ymcIyzw",
            authDomain: "film2watch-696be.firebaseapp.com",
            databaseURL: "https://film2watch-696be-default-rtdb.europe-west1.firebasedatabase.app",
            projectId: "film2watch-696be",
            storageBucket: "film2watch-696be.appspot.com",
            messagingSenderId: "235488541940",
            appId: "1:235488541940:web:0f34a46cfd46bc3d0832a1"
        };

        firebase.initializeApp(firebaseConfig);
        const dbRef = firebase.database().ref();
        const listRef = dbRef.child('list');
        
        
        const APIKEY ="670e76a2df83c93710cbc7ed9fa21477"
        let baseURL = 'https://api.themoviedb.org/3/';
        let baseImgURL = 'https://image.tmdb.org/t/p/';
        const searchbar = document.querySelector('.search-input');
        let timeout = null;
        let count = 0;
        var myListfilm = [];

        function search(keyword) {
            if(keyword == ''){
                viewList();
            }else{

            count = 0;
            document.getElementById('listFilm').style.display = 'none'
            document.getElementById("list").remove();
            let div = document.createElement("div");
            div.setAttribute("class","list");
            div.setAttribute("id","list");
            document.body.appendChild(div);
            let url = ''.concat(baseURL, 'search/movie?api_key=', APIKEY, '&query=', keyword);
            fetch(url)
            .then(result=>result.json())
            .then((data)=>{
                let listId = getId(data);
                for (let i = 0; i < listId.length; i++) {
                    getImg(listId[i]);
                }
                
                
            })
            }

            
        }
        searchbar.addEventListener('keyup', () =>{
            clearTimeout(timeout);
            timeout = setTimeout(function (){
                search(searchbar.value);
            }, 500);
        });
        function getId(data){
            let listId = [];
            for (let i = 0; i < data.results.length; i++) {
                listId.push(data.results[i].id);
                
            }
            return listId;
        }
        function getImg(id){
            let url = ''.concat("https://api.themoviedb.org/3/movie/",id,'/images?api_key=',APIKEY, "&language=en-US&include_image_language=fr");
            fetch(url)
            .then(result=>result.json())
            .then((data)=>{
                let path = data.posters[0].file_path;
                let size = 'w500';

                let newDiv = document.createElement('div');
                newDiv.setAttribute("class","image");
                newDiv.setAttribute("id","image");
                document.getElementById("list").appendChild(newDiv);

                let newImg = document.createElement('div');
                let urlImg = ''.concat(baseImgURL, size, path);
                newImg.setAttribute("class",'div-image');
                newImg.setAttribute('id', count.toString());
                newImg.setAttribute('onclick','addList(this)')
                newImg.style.backgroundImage = ''.concat('url(',urlImg,')');
                document.getElementById("image").appendChild(newImg);

                let button = document.createElement('a');
                button.textContent = '+'
                document.getElementById(count.toString()).appendChild(button);

                count++;
                
            });
        }
        function addList(element){
            
            myListfilm = [];
            let img = element.style.backgroundImage;
            img = img.slice(5,img.length-2);
            getListFromDB();

            if(myListfilm.indexOf(img) == -1){
                listRef.push(img);
            }

            
        }

        function viewList(){
            myListfilm = [];
            
            document.getElementById("list").remove();
            let div = document.createElement("div");
            div.setAttribute("class","list");
            div.setAttribute("id","list");
            document.body.appendChild(div);

            document.getElementById('listFilm').remove();
            let newDiv = document.createElement('div');
            newDiv.setAttribute('class', 'listFilm')
            newDiv.setAttribute('id', 'listFilm')
            document.getElementById('myList').appendChild(newDiv);

            getListFromDB();

            for (let i = 0; i < myListfilm.length; i++) {
                 let newFilm = document.createElement('div');
                 newFilm.setAttribute('class', 'div-image');
                 newFilm.setAttribute('id', ''.concat('film',i))
                 newFilm.setAttribute('onclick','removeList(this)')
                 newFilm.style.backgroundImage = ''.concat('url(', myListfilm[i], ')');
                 let button = document.createElement('a');
                 button.textContent = '-'
                 document.getElementById('listFilm').appendChild(newFilm);
                 document.getElementById(''.concat('film',i)).appendChild(button);
                
            }
            document.getElementById('listFilm').style.display = 'flex';
        }

        function removeList(element){

            listRef.remove();
            let film = element.style.backgroundImage.toString();
            film = film.slice(5,film.length-2);
            let index = myListfilm.indexOf(film);
            myListfilm.splice(index,1);
            let temp = [];
            for (let i = 0; i < myListfilm.length; i++) {
                temp.push(myListfilm[i]);
            }
            
            for (let i = 0; i < temp.length; i++) {
                listRef.push(temp[i]);
                
            }


            viewList();
        }

        function getListFromDB(){
            listRef.on('child_added', snap =>{
                let data = snap.val();
                myListfilm.push(data);
                });
        }

        function retourList(){
            searchbar.value = ''
            viewList();
        }

        window.addEventListener('load', () =>{
            
            viewList();
            document.getElementById('listFilm').style.display = 'flex';
            
        })
